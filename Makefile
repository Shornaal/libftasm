# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/15 19:26:04 by tiboitel          #+#    #+#              #
#    Updated: 2015/06/13 21:01:25 by tiboitel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libfts.a

FILES = ft_isalpha.s \
		ft_isdigit.s \
		ft_isprint.s \
		ft_isascii.s \
		ft_isalnum.s \
		ft_isupper.s \
		ft_isspace.s \
		ft_tolower.s \
		ft_toupper.s \
		ft_strlen.s \
		ft_puts.s \
		ft_strcat.s \
		ft_bzero.s \
		ft_memset.s \
		ft_memcpy.s \
		ft_strdup.s \
		ft_putstr.s \
		ft_strcpy.s \
		ft_strncpy.s \
		ft_memalloc.s \
		ft_strnew.s \
		ft_cat.s

PATH_SRCS = srcs/
PATH_SPEC = specs/
AS = nasm
SFLAGS = -fmacho64 -DOSX --prefix _
AR = ar rcs
SRCS = $(addprefix $(PATH_SRCS), $(FILES))
OBJS = $(SRCS:.s=.o)

all: $(NAME)

$(NAME): $(OBJS)
		$(AR) $(NAME) $(OBJS)

%.o: %.s
		$(AS) $(SFLAGS) $< -o $@

clean:
		rm -rf $(OBJS)

fclean: clean
		rm -rf $(NAME)

re: fclean all

specs: $(NAME)
		@make -C $(PATH_SPEC) run

.PHONY: clean fclean re all
