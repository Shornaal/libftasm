; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_isalpha.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/05/15 19:28:26 by tiboitel          #+#    #+#              #
;    Updated: 2015/05/15 19:50:01 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section .text
	global ft_isspace

ft_isspace:
	enter 0,0
	cmp rdi, 32
	je end
	cmp rdi, 9
	je end
	cmp rdi, 11
	je end
	cmp rdi, 10
	je end
	cmp rdi, 13
	je end
	cmp rdi, 12
	je end
	mov rax, 0
	leave
	ret

end:
	mov rax, 1
	leave
	ret
