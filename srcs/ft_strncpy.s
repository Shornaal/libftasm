# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strncpy.s                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/06/12 23:17:10 by tiboitel          #+#    #+#              #
#    Updated: 2015/06/12 23:30:14 by tiboitel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section .text
	global ft_strncpy

ft_strncpy:
	enter 0, 0
	cmp rdi, 0x0
	je isnull
	cmp rsi, 0x0
	je isnull
	push rdi
	cmp rdx, 0x0
	je end

cpy:
	cmp rdx, 0x0
	je end
	cmp byte [rsi], 0x0
	je fill
	mov al, [rsi]
	mov [rdi], al
	dec rdx
	inc rdi
	inc rsi
	jmp cpy

fill:
	cmp rdx, 0x0
	je end
	mov al, 0x0
	mov [rdi], al
	dec rdx
	inc rdi
	jmp fill

isnull:
	mov rax, 0x0
	leave
	ret

end:
	pop rax
	leave
	ret
