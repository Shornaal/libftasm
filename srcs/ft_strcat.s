; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_strcat.s                                        :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/06/03 21:07:35 by tiboitel          #+#    #+#              #
;    Updated: 2015/06/03 21:07:35 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section	.text
	global ft_strcat

ft_strcat:
	enter 0, 0
	cmp rsi, 0x0
	jz end
	mov	rax, rdi ; set arg to return value

itsend:
	cmp byte [rdi], 0x0
	jz append ; jump if zero
	inc rdi
	jmp itsend ; loop

append:
	cmp BYTE [rsi], 0x0
	jz end
	movsb ; move byte rsi to rdi
	jmp append

end:
	mov BYTE [rdi], 0x0 ; add NULL to end of str
	leave
	ret

