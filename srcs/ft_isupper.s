; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_isupper.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/06/13 19:50:58 by tiboitel          #+#    #+#              #
;    Updated: 2015/06/13 19:50:58 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section .text
	global ft_isupper

ft_isupper:
	enter 0,0
	mov eax, 0x1
	cmp rdi, 0x41
	jl false
	cmp rdi, 0x5a
	jg false
	jmp end

false:
	mov eax, 0x0

end:
	leave
	ret
