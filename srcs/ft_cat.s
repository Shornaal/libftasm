# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_cat.s                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/06/12 21:08:40 by tiboitel          #+#    #+#              #
#    Updated: 2015/06/12 21:15:37 by tiboitel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

%define READ		0x2000003
%define WRITE		0x2000004
%define BUFF_SIZE	64

section	.text
	global ft_cat

ft_cat:
	enter BUFF_SIZE, 0
	cmp rdi, 0x-1
	je end
	mov r8, rdi ; save of args1

read:
	mov rdi, r8
	mov rax, READ
	mov rsi, rsp ; save
	mov rdx, BUFF_SIZE
	syscall
	jc end
	cmp rax, 0x0
	jz end
	
print:
	mov rdx, rax
	mov rax, WRITE
	mov rdi, 1
	mov rsi, rsp
	syscall
	jc end
	jmp read

end:
	leave
	ret
