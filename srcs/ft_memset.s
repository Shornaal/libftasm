; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_memset.s                                        :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/06/03 21:12:30 by tiboitel          #+#    #+#              #
;    Updated: 2015/06/03 21:12:30 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section	.text
	global ft_memset
	
ft_memset:
	enter 0, 0
	push rbp
	mov rbp, rsp
	push rdi
	mov rcx, rdx
	mov rax, rsi
	cld
	rep stosb

end:
	pop rax
	pop	rbp
	leave
	ret
