; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_isprint.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/05/19 17:05:37 by tiboitel          #+#    #+#              #
;    Updated: 2015/05/19 17:05:37 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section		.text
	global ft_isprint

ft_isprint:
	enter 0,0
	mov eax, 0x1
	cmp rdi, 0x20
	jl false
	cmp rdi, 0x7e
	jg false
	jmp end

false:
	mov eax, 0x0

end:
	leave
	ret
