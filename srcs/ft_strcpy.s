; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_strcpy.s                                        :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/06/12 22:54:18 by tiboitel          #+#    #+#              #
;    Updated: 2015/06/12 22:54:18 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section .text
	global ft_strcpy

ft_strcpy:
	enter 0, 0
	cmp rsi, 0x0
	jz null
	cmp rdi, 0x0
	jz null
	mov rbx, rdi
	mov rax, rdi

loop:
	cmp BYTE [rsi], 0x0
	jz end
	mov bl, [rsi]
	mov [rdi], bl
	inc rdi
	inc rsi
	jmp loop

null:
	mov rax, 0x0
	leave
	ret

end:
	mov BYTE [rdi], 0x0
	leave
	ret

