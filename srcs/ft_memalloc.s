; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_memalloc.s                                      :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/06/12 23:31:06 by tiboitel          #+#    #+#              #
;    Updated: 2015/06/12 23:31:06 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section	.text
	global ft_memalloc
	extern ft_bzero
	extern malloc

ft_memalloc:
	enter 0, 0
	mov rsi, rdi
	push rsi
	push rdi
	call malloc
	pop rdi
	pop rsi
	mov rdi, rax
	call ft_bzero

end:
	leave
	ret
