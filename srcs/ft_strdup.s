; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_strdup.s                                        :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/06/12 19:25:44 by tiboitel          #+#    #+#              #
;    Updated: 2015/06/12 19:25:44 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section	.text
	global ft_strdup
	extern malloc
	extern ft_strlen

ft_strdup:
	enter 0,0
	cmp rdi, 0x0
	jz rnull
	push rbx
	mov rbx, rdi ; move address str, rbx 
	call ft_strlen

calloc:
	add rax, 0x1
	mov rdi, rax
	push rdi
	call malloc
	pop rdi
	cmp rax, 0x0
	jz end

cpy:
	mov rcx, rdi
	mov rdi, rax
	mov rsi, rbx
	cld
	rep movsb

end:
	pop rbx
	leave
	ret

rnull:
	mov rax, 0x0
	leave


