; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_isalnum.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/05/19 17:11:00 by tiboitel          #+#    #+#              #
;    Updated: 2015/05/19 17:11:00 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section		.text
	extern	ft_isalpha
	extern	ft_isdigit
	global	ft_isalnum

ft_isalnum:
	enter 0, 0
	call	ft_isdigit
	cmp	eax, 0x1
	je	end
	call	ft_isalpha

end:
	leave
	ret
