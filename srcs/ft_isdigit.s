; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_isdigit.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/05/19 17:01:21 by tiboitel          #+#    #+#              #
;    Updated: 2015/05/19 17:01:21 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section .text
	global ft_isdigit

ft_isdigit:
	enter 0, 0
	mov eax, 0x1
	cmp rdi, 0x30
	jl false
	cmp rdi, 0x39
	jg false
	jmp end

false:
	mov eax, 0x0

end:
	leave
	ret
