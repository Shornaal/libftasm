; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_isalpha.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/05/15 19:28:26 by tiboitel          #+#    #+#              #
;    Updated: 2015/05/15 19:50:01 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section .text
	global ft_isalpha

ft_isalpha:
	enter 0,0
	mov eax, 0x1
	cmp rdi, 0x41
	jl false
	cmp rdi, 0x7a
	jg false
	cmp rdi, 0x5a
	jbe end
	cmp rdi, 0x61
	jae end

false:
	mov eax, 0x0

end:
	leave
	ret
