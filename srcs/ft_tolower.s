; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_tolower.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/05/19 19:05:23 by tiboitel          #+#    #+#              #
;    Updated: 2015/05/19 19:05:23 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section		.text
	global	ft_tolower

ft_tolower:
	enter 0, 0
	mov eax, edi
	cmp edi, 0x41
	jl end
	cmp edi, 0x5a
	ja end

downcase:
	add eax, 0x20

end:
	leave
	ret
