; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_memcpy.s                                        :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/06/03 21:18:24 by tiboitel          #+#    #+#              #
;    Updated: 2015/06/03 21:18:24 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section	.text
	global ft_memcpy

ft_memcpy:
	enter 0, 0
	mov rax, rdi
	mov rcx, rdx
	rep movsb

end:
	leave
	ret
