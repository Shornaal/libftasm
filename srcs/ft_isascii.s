; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_isascii.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/05/19 17:07:35 by tiboitel          #+#    #+#              #
;    Updated: 2015/05/19 17:07:35 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section		.text
	global	ft_isascii

ft_isascii:
	enter 0, 0
	mov eax, 0x1
	cmp rdi, 0x0
	jl false
	cmp rdi, 0x7F
	jg false
	jmp end

false:	
	mov eax, 0x0

end:
	leave
	ret
