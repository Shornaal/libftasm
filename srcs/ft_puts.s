; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_puts.s                                          :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/06/03 19:26:10 by tiboitel          #+#    #+#              #
;    Updated: 2015/06/03 19:26:10 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

; return 10 si marche
section .data
	nl		db 10

section .rodata
	nullq: db "(null)", 10, 0

section	.text
	global	ft_puts

ft_puts:
	enter 8, 0
	mov rax, -1
	cmp rdi, 0x0
	je null
	mov r8, rdi

loop:
	cmp BYTE [r8], 0x0
	je newline
	mov rax, 0x2000004 ; write	
	mov rdi, 0x1
	mov rsi, r8 ; adress to give to write
	mov rdx, 0x1 ; length to write
	syscall ; syscall correspond to kernel value move into rax (write)
	jc end
	inc	r8
	jmp loop

newline:
	mov rax, 0x2000004
	mov rdi, 0x1
	lea rsi, [rel nl]
	mov rdx, 0x1
	syscall
	jc end
	mov rax, 10
	jmp end

null:
	mov rax, 0x2000004
	mov rdi, 0x1
	lea rsi, [rel nullq]
	mov rdx, 0x10
	syscall
	jc end
	mov rax, 0x-1
	jmp end	


end:
	leave
	mov rax, 10
	ret
