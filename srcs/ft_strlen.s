; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_strlen.s                                        :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/05/19 19:41:02 by tiboitel          #+#    #+#              #
;    Updated: 2015/05/19 19:41:02 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section		.text
	global	ft_strlen

ft_strlen:
	enter 0,0
	mov ecx, 0x-1
	mov al, 0x0
	; clear direction flag
	cld
	; repeat where not equal / scan byte string
	repne scasb
	; Avance compteur, recule index
	; eax = cmptr, - -2 pour passer positif
	mov eax, 0x-2
	sub eax, ecx

end:
	leave
	ret
