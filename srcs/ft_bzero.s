; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_bzero.s                                         :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/06/03 19:04:08 by tiboitel          #+#    #+#              #
;    Updated: 2015/06/03 19:04:08 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section .text
	global	ft_bzero

ft_bzero:
	enter 0, 0
	mov rbx, rdi
	mov rcx, rsi

loop:
	cmp rcx, 0x0
	je end
	mov BYTE [rbx], 0x0
	inc rbx
	dec rcx
	jmp loop

end:
	leave
	ret
