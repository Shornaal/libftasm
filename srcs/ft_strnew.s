# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_strnew.s                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/06/13 19:41:06 by tiboitel          #+#    #+#              #
#    Updated: 2015/06/13 20:54:39 by tiboitel         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

section	.text
	global ft_strnew
	extern ft_bzero
	extern malloc

ft_strnew:
	enter 0, 0
	inc rdi
	mov rsi, rdi
	push rsi
	push rdi
	call malloc
	pop rdi
	pop rsi
	mov rdi, rax
	call ft_bzero

end:
	leave
	ret
