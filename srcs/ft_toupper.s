; **************************************************************************** #
;                                                                              #
;                                                         :::      ::::::::    #
;    ft_toupper.s                                       :+:      :+:    :+:    #
;                                                     +:+ +:+         +:+      #
;    By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+         #
;                                                 +#+#+#+#+#+   +#+            #
;    Created: 2015/05/19 19:00:02 by tiboitel          #+#    #+#              #
;    Updated: 2015/05/19 19:00:02 by tiboitel         ###   ########.fr        #
;                                                                              #
; **************************************************************************** #

section		.text
	global	ft_toupper

ft_toupper:
	enter 0, 0
	mov eax, edi
	cmp edi, 0x61
	jl end
	cmp edi, 0x7a
	ja end
	
upcase:
	sub eax, 0x20

end:
	leave
	ret
