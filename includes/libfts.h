/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libfts.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tiboitel <tiboitel@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/16 17:56:21 by tiboitel          #+#    #+#             */
/*   Updated: 2015/06/13 20:34:57 by tiboitel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIBFTS_H
# define FT_LIBFTS_H
# include <sys/types.h>

int		ft_isalpha(int c);
int		ft_isdigit(int c);
int		ft_isprint(int c);
int		ft_isascii(int c);
int		ft_isalnum(int c);
int		ft_isupper(int c);
int		ft_isspace(int c);
int		ft_toupper(int c);
int		ft_tolower(int c);
int		ft_strlen(const char *s);
int		ft_puts(const char *s);
int		ft_putstr(const char *s);
char	*ft_strcat(char *s1, const char *s2);
void	ft_bzero(void *s, size_t n);
void	ft_memset(void *b, int c, size_t len);
void	*ft_memcpy(void *dst, const void *src, size_t n);
void	ft_cat(int fd);
char	*ft_strdup(const char *s1);
char	*ft_strcpy(const char *src, char *dst);
char	*ft_strcpy(const char *src, char *dst, size_t n);
char	*ft_strnew(size_t size);
void	*ft_memalloc(size_t n);
#endif
